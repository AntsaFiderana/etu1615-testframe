<%@ page import="java.util.List" %>
<%@ page import="com.example.test.controller.Person" %><%--
  Created by IntelliJ IDEA.
  User: ASUS
  Date: 12/11/2022
  Time: 06:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    Person people = (Person) request.getAttribute("person");
%>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <h3>Welcome to your dashboard</h3>
    <h5><strong>ID : </strong><%= people.getId() %></h5>
    <h5><strong>Name : </strong><%= people.getName() %></h5>
    <h5><strong>Birth : </strong><%= people.getBirth() %></h5>
    <h5><strong>Major : </strong><%= people.isMajor() %></h5>
    <h5><strong>Entreprise : </strong><%= people.getEntreprise().getId() %></h5>
</body>
</html>
