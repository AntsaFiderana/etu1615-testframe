package com.example.test.controller;

public class Entreprise {
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Entreprise(int id){
        this.id = id;
    }
}
