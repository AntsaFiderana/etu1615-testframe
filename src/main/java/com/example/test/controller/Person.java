package com.example.test.controller;


import com.example.spring.core.ViewModel;
import com.example.spring.core.annotation.Controller;
import com.example.spring.core.annotation.RequestMapping;

import java.time.LocalDate;

@Controller
public class Person {
    private int id;

    private String name;

    private LocalDate birth;

    private boolean major;

    private Entreprise entreprise;

    @RequestMapping(name = "person-find")
    public ViewModel find(){
        ViewModel viewModel = new ViewModel();
        viewModel.setView("person");
        viewModel.addAttribute("person", new Person(1,"Antsa",LocalDate.now(), new Entreprise(2)));
        return viewModel;
    }

    @RequestMapping(name = "person-save-form")
    public ViewModel saveForm(){
        ViewModel viewModel = new ViewModel();
        viewModel.setView("person-save");
        return viewModel;
    }

    @RequestMapping(name = "person-save")
    public ViewModel save(){
        ViewModel viewModel = new ViewModel();
        viewModel.setView("person");
        viewModel.addAttribute("person", this);
        return viewModel;
    }

    @RequestMapping(name = "fsgg")
    public void top(){

    }

    public Person(){}

    public Person(int id, String name, LocalDate birth, Entreprise entreprise) {
        this.id = id;
        this.name = name;
        this.birth = birth;
        this.entreprise = entreprise;
    }

    public boolean isMajor() {
        return major;
    }

    public void setMajor(boolean major) {
        this.major = major;
    }

    public LocalDate getBirth() {
        return birth;
    }

    public void setBirth(LocalDate birth) {
        this.birth = birth;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Entreprise getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(Entreprise entreprise) {
        this.entreprise = entreprise;
    }
}
